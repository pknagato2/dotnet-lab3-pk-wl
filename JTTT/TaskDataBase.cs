﻿using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace JTTT
{
    class TaskDataBase
    {
        public TaskDataBase()
        {
            db = new TaskDbContext();
        }
        public void AddTaskToDataBase(Task task)
        {
                db.Tasks.Add(task);
                SaveDb();
        }
        public List<Task> GetAllTasks()
        {
                List<Task> tasks = new List<Task>();
                foreach (var task in db.Tasks)
                {
                    tasks.Add(task);
                }
                return tasks;
        }
        public void RemoveTask(Task task)
        {
            db.Tasks.Attach(task);
            db.Tasks.Remove(task);
            SaveDb();
        }

        public void RemoveAll()
        {
            var tasks = GetAllTasks();
            for (int i = tasks.Count - 1; i > -1; i--)
            {
                RemoveTask(tasks[i]);
            }
        }

        private void SaveDb()
        {
            bool saveFailed = false;
            do
            {
                saveFailed = false;
                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    saveFailed = true;
                    ex.Entries.Single().Reload();
                }
            } while (saveFailed);
        }

        private TaskDbContext db;
    }
}
