﻿using System.Data.Entity;


namespace JTTT
{
    class TaskDbContext : DbContext
    {
        public TaskDbContext() : base("TaskKLs")
        {}

        public DbSet<Task> Tasks { get; set; }
    }
}
