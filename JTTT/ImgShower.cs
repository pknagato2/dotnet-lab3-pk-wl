﻿using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Net;
using System.Windows.Forms;

namespace JTTT
{
    class ImgGetter
    {
        public static Image GetImage(string address)
        {
            WebClient wc = new WebClient();
            byte[] bytes = wc.DownloadData(address);
            MemoryStream ms = new MemoryStream(bytes);
            System.Drawing.Image img = System.Drawing.Image.FromStream(ms);
            return img;
        }
    }

    class ImgShower
    {
        public static void Show(List<string> addresses)
        {
            foreach (var address in addresses)
            {
                var img = ImgGetter.GetImage(address);    
                var pb = new weatherInfo();
                pb.Show();
                pb.showImg(img, address);
                
                Logger.LogMessage("Img from: " + address + "has been shown");
            }
        }
    }
}
