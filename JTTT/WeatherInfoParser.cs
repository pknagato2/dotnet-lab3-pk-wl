﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Weather;

namespace JTTT
{
    class WeatherInfoParser
    {
        public Tuple<string,Image> Parse(string City)
        {
            string infoText = "";
            try
            {
                var weatherJson = GetJsonForCity(City);
                infoText = "Dzisiaj jest " + (weatherJson.Main.Temp - 273.15) + " stopni Celcjusza.";
                infoText += "\nCiśnienie " + weatherJson.Main.Pressure + " hPa.";
                infoText += "\nCo na niebie? " + weatherJson.Weather[0].Description + ".";
                image = ImgGetter.GetImage("http://openweathermap.org/img/w/" + weatherJson.Weather[0].Icon.ToString() + ".png");
                
                return new Tuple<string, Image>(infoText, image);
            }
            catch
            {
                return new Tuple<string, Image>($"Nie znaleziono danych dla miasta {City}", image);
            }
        }

        public double getTemp(string City)
        {
            var weatherJson = GetJsonForCity(City);
            return weatherJson.Main.Temp;
        }

        private WeatherJson GetJsonForCity(string City)
        {
            using (var wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                var json = System.Net.WebUtility.HtmlDecode(
                    wc.DownloadString($"http://api.openweathermap.org/data/2.5/weather?q={City},pl&APPID={APPID}"));
                var weatherJson = WeatherJson.FromJson(json);
                return weatherJson;
            }
        }

        private Image image;
        private readonly string APPID = "3dac5c051cd7009da8ef28e486245c7a";
    }
}
