﻿using System;
using System.Windows.Forms;

namespace JTTT
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
            taskList = new TaskList();
            this.taskListBox.DataSource = taskList.GetTaskList();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (conditionTab.SelectedIndex == 0 )
                {
                    if (CheckFilledWebsiteParams()) return;
                    if (actionTab.SelectedIndex == 1)
                    {
                        taskList.AddTask(TaskBuilder.createImgShowTask(textBox.Text.ToString(), urlBox.Text.ToString()));
                    }
                    else if (actionTab.SelectedIndex == 0)
                    {
                        if (!IsMailSet())
                            return;
                        taskList.AddTask(TaskBuilder.createImgSendTask(textBox.Text.ToString(), urlBox.Text.ToString(), adressBox.Text));
                    }
                }
                else if (conditionTab.SelectedIndex == 1)
                {
                    if (CheckWeatherParams()) return;
                    if (actionTab.SelectedIndex == 0)
                    {
                        if (!IsMailSet())
                            return;
                        taskList.AddTask(TaskBuilder.createWeatherInfoSendTask(townName.Text.ToString(),
                            Convert.ToDouble(tempText.Text),
                            adressBox.Text));
                    }
                    else if (actionTab.SelectedIndex == 1)
                    {
                        taskList.AddTask(TaskBuilder.createWeatherInfoShowTask(townName.Text.ToString(),
                            Convert.ToDouble(tempText.Text)));
                    }
                }
            }
            catch { }
        }

        private bool CheckFilledWebsiteParams()
        {
            var res = IsUrlSet();
            var res2 = IsTextSet();
            if (!res || !res2) return true;
            CheckUrl();
            return false;
        }

        private bool CheckWeatherParams()
        {
            var res = IsCitySet();
            var res2 = IsTempSet();
            if (!res || !res2) return true;
            return false;
        }

        private bool IsCitySet()
        {
            if (townName.Text == "")
            {
                cityAlarm.Visible = true;
                return false;
            }
            cityAlarm.Visible = false;
            return true;
        }
        private bool IsTempSet()
        {
            if (tempText.Text == "")
            {
                tempAlarm.Visible = true;
                return false;
            }
            tempAlarm.Visible = false;
            return true;
        }
        private void urlBox_TextChanged(object sender, EventArgs e)
        {
            IsUrlSet();
        }

        private void textBox_TextChanged(object sender, EventArgs e)
        {
            IsTextSet();
        }

        private bool IsUrlSet()
        {
            if (urlBox.Text == "")
            {
                urlAlarm.Visible = true;
                return false;
            }
            urlAlarm.Visible = false;
            return true;
        }

        private void CheckUrl()
        {
            if (!(urlBox.Text.StartsWith("https://") || urlBox.Text.StartsWith("http://")))
            {
                urlBox.Text = "https://" + urlBox.Text;
            }
        }

        private bool IsTextSet()
        {
            if (textBox.Text == "")
            {
                textAlarm.Visible = true;
                return false;
            }
            textAlarm.Visible = false;
            return true;
        }
        private bool IsMailSet()
        {
            if (adressBox.Text == "" && adressBox.Visible)
            {
                adressAlarm.Visible = true;
                return false;
            }
            adressAlarm.Visible = false;
            return true;
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            taskList.ClearTasks();
        }

        private void actButton_Click(object sender, EventArgs e)
        {
            taskList.ExecuteAll();
        }

        private void deserializeButton_Click(object sender, EventArgs e)
        {
            taskList.LoadFromFile();
        }

        private void serializeButton_Click(object sender, EventArgs e)
        {
            taskList.WriteTofile();
        }

        private void weatherButton_Click(object sender, EventArgs e)
        {
            var weatherForm = new WeatherForm();
            weatherForm.ShowDialog();
        }

        private void adressBox_TextChanged(object sender, EventArgs e)
        {
            if (adressBox.Text != "")
            {
                adressAlarm.Visible = false;
            }
            else
            {
                adressAlarm.Visible = true;
            }
        }

        private void townName_TextChanged(object sender, EventArgs e)
        {
            if (townName.Text != "")
            {
                cityAlarm.Visible = false;
            }
            else
            {
                cityAlarm.Visible = true;
            }
        }

        private void tempText_TextChanged(object sender, EventArgs e)
        {
            if (tempText.Text != "")
            {
                tempAlarm.Visible = false;
            }
            else
            {
                tempAlarm.Visible = true;
            }
        }
    }
}
