﻿namespace JTTT
{
    public static class TaskBuilder
    {
        public static Task createImgSendTask(string key, string url, string mailAddress)
        {
            Task task = new Task(Task.TaskType.eImgSendTask, key, url, mailAddress);
            return task;
        }

        public static Task createImgShowTask(string key, string url)
        {
            Task task = new Task(Task.TaskType.eImgShowTask, key, url, "none");
            return task;
        }

        public static Task createWeatherInfoShowTask(string townName, double temp)
        {
            return new Task(Task.TaskType.eWeatherShowTask, "", "", "", townName, temp);
        }

        public static Task createWeatherInfoSendTask(string townName, double temp, string mailAddress)
        {
            return new Task(Task.TaskType.eWeatherSendTask, "", "", mailAddress, townName, temp);
        }
    }
}
