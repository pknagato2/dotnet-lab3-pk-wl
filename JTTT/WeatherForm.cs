﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using Weather;

namespace JTTT
{
    public partial class WeatherForm : Form
    {
        public WeatherForm()
        {
            InitializeComponent();
        }

        private void checkWeatherButton_Click(object sender, EventArgs e)
        {
            var parsedInfo = new WeatherInfoParser().Parse(choosenCity.Text);
            infoText.Text = parsedInfo.Item1;
            weatherPic.Image = parsedInfo.Item2;
        }

    }
}
