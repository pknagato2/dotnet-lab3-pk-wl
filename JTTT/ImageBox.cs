﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JTTT
{
    public partial class weatherInfo : Form
    {

        public weatherInfo()
        {
            InitializeComponent();
        }

        public void showInfo(string city)
        {
            var parsedInfo = new WeatherInfoParser().Parse(city);
            infoText.Text = parsedInfo.Item1;
            pictBox.Image = parsedInfo.Item2;
        }

        public void showImg(Image img, string text)
        {
            pictBox.Image = img;
            infoText.Text = text;
        }

      
    }
}
