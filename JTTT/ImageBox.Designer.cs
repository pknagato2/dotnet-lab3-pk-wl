﻿namespace JTTT
{
    partial class weatherInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.infoText = new System.Windows.Forms.RichTextBox();
            this.pictBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictBox)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 147);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "załącznik";
            // 
            // infoText
            // 
            this.infoText.Location = new System.Drawing.Point(16, 12);
            this.infoText.Name = "infoText";
            this.infoText.Size = new System.Drawing.Size(661, 121);
            this.infoText.TabIndex = 1;
            this.infoText.Text = "";
            // 
            // pictBox
            // 
            this.pictBox.Location = new System.Drawing.Point(14, 212);
            this.pictBox.Name = "pictBox";
            this.pictBox.Size = new System.Drawing.Size(663, 483);
            this.pictBox.TabIndex = 2;
            this.pictBox.TabStop = false;
            // 
            // weatherInfo
            // 
            this.ClientSize = new System.Drawing.Size(691, 707);
            this.Controls.Add(this.pictBox);
            this.Controls.Add(this.infoText);
            this.Controls.Add(this.label2);
            this.Name = "weatherInfo";
            ((System.ComponentModel.ISupportInitialize)(this.pictBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion


        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox infoText;
        private System.Windows.Forms.PictureBox pictBox;
    }
}