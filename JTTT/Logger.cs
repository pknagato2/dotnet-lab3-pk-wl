﻿using System;
using System.IO;

namespace JTTT
{
    internal static class Logger
    { 

        public static void LogMessage(string message)
        {
            using (StreamWriter w = File.AppendText("log.txt"))
            {
                string stringToLog = DateTime.Now.ToString("h:mm:ss tt") + " " + message;
                w.WriteLine(stringToLog);
            }

        }

    }
}
