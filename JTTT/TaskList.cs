﻿using System.ComponentModel;
using System.IO;
using System.Xml.Serialization;

namespace JTTT
{
    public class TaskList
    {
        private BindingList<Task> list = new BindingList<Task>();
        private TaskDataBase tdb = new TaskDataBase();

        public TaskList()
        {
            list = new BindingList<Task>();
            LoadTasksFromDb();
        }

        public void AddTask(Task task)
        {
            tdb.AddTaskToDataBase(task);
            list.Add(task);
        }

        public BindingList<Task> GetTaskList()
        {
            return list;
        }

        public void DeleteTask(Task task)
        {
            tdb.RemoveTask(task);
            list.Remove(task);
        }

        public void LoadFromFile()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(BindingList<Task>));
            using (FileStream fs = File.OpenRead("TaskList.xml"))
            {
                var tmpList = (BindingList<Task>)serializer.Deserialize(fs);
                foreach(var x in tmpList)
                {
                    AddTask(x);
                }
            }

        }

        public void WriteTofile()
        {
            using (Stream fs = new FileStream("TaskList.xml",
                FileMode.Create, FileAccess.Write, FileShare.None))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(BindingList<Task>));
                serializer.Serialize(fs, list);
            }
        }

        public void ClearTasks()
        {
            list.Clear();
            tdb.RemoveAll();
        }

        public void ExecuteAll()
        {
            for(int i = list.Count -1; i> -1; i--)
            {
                list[i].Execute();
                //DeleteTask(list[i]);
            }
        }

        private void LoadTasksFromDb()
        {
            foreach (var task in tdb.GetAllTasks())
            {
                list.Add(task);
            }
        }
    }
}
