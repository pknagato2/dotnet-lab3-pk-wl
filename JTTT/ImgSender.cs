﻿using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Mail;

namespace JTTT
{
    class ImgSender
    {
        public void Send(string receiverAddress, List<string> imgAddresses)
        {
            List<Attachment> attachments = new List<Attachment>();
            foreach (var imgAddress in imgAddresses)
            {
                attachments.Add(GetAttachmentFromAdress(imgAddress));
            }
            var mailSender = new MailSender();
            mailSender.Send(receiverAddress, attachments);
            Logger.LogMessage("Images have been sent to: " + receiverAddress);
        }

        private Attachment GetAttachmentFromAdress(string address)
        {
            WebClient wc = new WebClient();
            byte[] bytes = wc.DownloadData(address);
            MemoryStream ms = new MemoryStream(bytes);
            System.Net.Mime.ContentType ct = new System.Net.Mime.ContentType(System.Net.Mime.MediaTypeNames.Text.Plain);
            System.Net.Mail.Attachment attach = new System.Net.Mail.Attachment(ms, ct);
            attach.ContentDisposition.FileName = "image.jpg";
            return attach;
        }

    }

    public class MailSender
    {
        public void Send(string adress, List<Attachment> attach, string body = "")
        {
            SetReceiver(adress);
            var message = new MailMessage(fromAddress_, toAddress);
            message.Subject = subject_;
            message.Body = body;
            message.IsBodyHtml = true;
            foreach (var attachment in attach)
            {
                message.Attachments.Add(attachment);
            }
            smtp_.Send(message);
        }

        private void SetReceiver(string adress)
        {
            toAddress = new MailAddress(adress.ToString(), "To Name");
        }

        public MailSender()
        {
            smtp_ = new SmtpClient {Host = "smtp.gmail.com", Port = 587, EnableSsl = true, DeliveryMethod = SmtpDeliveryMethod.Network,
                                       UseDefaultCredentials = false, Credentials = new NetworkCredential(fromAddress_.Address, fromPassword_)};
        }

        private readonly MailAddress fromAddress_ = new MailAddress("dotnetcsharp2018@gmail.com", "From CSharpLab");
        private readonly string fromPassword_ = "Csharp2018";
        private readonly string subject_ = "Your Images";
        private readonly SmtpClient smtp_;
        private MailAddress toAddress;

    }
}