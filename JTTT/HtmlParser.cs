﻿using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Windows.Forms;
using HtmlDocument = HtmlAgilityPack.HtmlDocument;

namespace JTTT
{
    public class HtmlParser
    {
        public HtmlParser(string utl)
        {
            url_ = utl;
        }

        public string GetPageHtml()
        {
            using (var wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                var html = System.Net.WebUtility.HtmlDecode(wc.DownloadString(url_));
                return html;
            }
        }

        private readonly string url_;

        public List<string> GetImgAdressWithTitle(string title)
        {
            var doc = new HtmlDocument();
            List<string> imgs = new List<string>();
            try
            {
                var pageHtml = GetPageHtml();
                doc.LoadHtml(pageHtml);
                var nodes = doc.DocumentNode.Descendants("img");
                foreach (var node in nodes)
                {
                    if (node.GetAttributeValue("alt", "").Contains(title))
                    {
                        imgs.Add(node.GetAttributeValue("src", ""));
                    }

                }

                if (imgs.Count > 0)
                    Logger.LogMessage("Pomyslnie zaladowano " + imgs.Count + ".");
                else
                    Logger.LogMessage("nie znaleziono obrazka o danym kluczu.");
                return imgs;
            }
            catch
            {
                var warning = "Nie mozna zaladowac strony: " + url_;
                Logger.LogMessage(warning);
                MessageBox.Show(warning);
                throw;
            }
            
        }


    }

}
