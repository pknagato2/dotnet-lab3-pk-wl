﻿namespace JTTT
{
    partial class WeatherForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CityText = new System.Windows.Forms.Label();
            this.choosenCity = new System.Windows.Forms.TextBox();
            this.checkWeatherButton = new System.Windows.Forms.Button();
            this.infoText = new System.Windows.Forms.RichTextBox();
            this.weatherPic = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.weatherPic)).BeginInit();
            this.SuspendLayout();
            // 
            // CityText
            // 
            this.CityText.AutoSize = true;
            this.CityText.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CityText.Location = new System.Drawing.Point(16, 30);
            this.CityText.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.CityText.Name = "CityText";
            this.CityText.Size = new System.Drawing.Size(102, 31);
            this.CityText.TabIndex = 0;
            this.CityText.Text = "Miasto: ";
            // 
            // choosenCity
            // 
            this.choosenCity.Location = new System.Drawing.Point(129, 33);
            this.choosenCity.Margin = new System.Windows.Forms.Padding(4);
            this.choosenCity.Name = "choosenCity";
            this.choosenCity.Size = new System.Drawing.Size(441, 22);
            this.choosenCity.TabIndex = 1;
            // 
            // checkWeatherButton
            // 
            this.checkWeatherButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.checkWeatherButton.Location = new System.Drawing.Point(21, 80);
            this.checkWeatherButton.Margin = new System.Windows.Forms.Padding(4);
            this.checkWeatherButton.Name = "checkWeatherButton";
            this.checkWeatherButton.Size = new System.Drawing.Size(551, 44);
            this.checkWeatherButton.TabIndex = 2;
            this.checkWeatherButton.Text = "Jaka jest pogoda?";
            this.checkWeatherButton.UseVisualStyleBackColor = true;
            this.checkWeatherButton.Click += new System.EventHandler(this.checkWeatherButton_Click);
            // 
            // infoText
            // 
            this.infoText.Location = new System.Drawing.Point(21, 145);
            this.infoText.Margin = new System.Windows.Forms.Padding(4);
            this.infoText.Name = "infoText";
            this.infoText.Size = new System.Drawing.Size(371, 134);
            this.infoText.TabIndex = 3;
            this.infoText.Text = "";
            // 
            // weatherPic
            // 
            this.weatherPic.Location = new System.Drawing.Point(417, 145);
            this.weatherPic.Margin = new System.Windows.Forms.Padding(4);
            this.weatherPic.Name = "weatherPic";
            this.weatherPic.Size = new System.Drawing.Size(155, 135);
            this.weatherPic.TabIndex = 4;
            this.weatherPic.TabStop = false;
            // 
            // WeatherForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(595, 294);
            this.Controls.Add(this.weatherPic);
            this.Controls.Add(this.infoText);
            this.Controls.Add(this.checkWeatherButton);
            this.Controls.Add(this.choosenCity);
            this.Controls.Add(this.CityText);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "WeatherForm";
            this.Text = "WeatherForm";
            ((System.ComponentModel.ISupportInitialize)(this.weatherPic)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label CityText;
        private System.Windows.Forms.TextBox choosenCity;
        private System.Windows.Forms.Button checkWeatherButton;
        private System.Windows.Forms.RichTextBox infoText;
        private System.Windows.Forms.PictureBox weatherPic;
    }
}