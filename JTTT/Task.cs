﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Windows.Forms;

namespace JTTT
{
    [Serializable()]
    public class Task
    {
        public enum TaskType {
            eEmptyTask,
            eImgSendTask,
            eImgShowTask,
            eWeatherSendTask,
            eWeatherShowTask
        };

        public int Id { get; set; }
        public string SearchKey { get; set; }
        public string MailAdress { get ; set; }
        public string Url { get ; set; }
        public string TownName { get; set; }
        public double Temp { get; set; }
        public TaskType taskType { get; set; }

        public Task() { }

        public Task(TaskType taskType, string searchKey, string url, string mailAdress, string townName="", double temp=-100) {
            this.taskType = taskType;
            this.SearchKey = searchKey;
            this.MailAdress = mailAdress;
            this.Url = url;
            this.Temp = temp;
            this.TownName = townName;
        }

        public List<string> GetImgNodes()
        {
            var htmlParser = new HtmlParser(Url);
            return htmlParser.GetImgAdressWithTitle(SearchKey);
        }

        public override string ToString()
        {
            if(taskType == TaskType.eImgSendTask)
            {
                return "Send imgs to " + MailAdress + "from url " + Url +" with searchkey : " + SearchKey;
            }
            else if (taskType == TaskType.eImgShowTask)
            {
                return "Showing image on page " + Url + " with searchkey : " + SearchKey;
            }
            else if (taskType == TaskType.eWeatherSendTask)
            {
                return "if temp is high than " + Temp + " send weather info from city: " + TownName + " to mail address :" + MailAdress ;
            }
            else if (taskType == TaskType.eWeatherShowTask)
            {
                return "if temp is high than " + Temp + " Show weather info in " + TownName;
            }
            else
            {
                return "this should'nt happen";
            }
        }

        public void Execute()
        {
            try
            {

                if (taskType == TaskType.eImgSendTask)
                    new ImgSender().Send(MailAdress, GetImgNodes());
                else if (taskType == TaskType.eImgShowTask)
                    ImgShower.Show(GetImgNodes());
                else if (Temp != -100 ) {
                    var temp = new WeatherInfoParser().getTemp(TownName);

                    if (temp -273.3 < Temp) return;

                    else if (taskType == TaskType.eWeatherShowTask)
                    {
                        var weatherInfo = new weatherInfo();
                        weatherInfo.Show();
                        weatherInfo.showInfo(TownName);
                    }
                    else if (taskType == TaskType.eWeatherSendTask)
                    {
                        var parsedInfo = new WeatherInfoParser().Parse(TownName);
                        new MailSender().Send(MailAdress, new List<System.Net.Mail.Attachment>(),
                                              "We wroclawiu " + parsedInfo.Item1.ToString());
                    }
                }
            }
            catch { }
            
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("searchKey", SearchKey);
            info.AddValue("mailAddress", MailAdress);
            info.AddValue("url", Url);
            info.AddValue("taskType", taskType);
            info.AddValue("TownName", TownName);
            info.AddValue("Temp", Temp);
        }

        public Task(SerializationInfo info, StreamingContext ctxt)
        {
            SearchKey = (string)info.GetValue("searchKey", typeof(string));
            MailAdress = (string)info.GetValue("mailAddress", typeof(string));
            Url = (string)info.GetValue("url", typeof(string));
            taskType = (TaskType)info.GetValue("taskType", typeof(TaskType));
            TownName = (string)info.GetValue("TownName", typeof(string));

        }
    }
}
