﻿namespace JTTT
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.serviceController1 = new System.ServiceProcess.ServiceController();
            this.urlBox = new System.Windows.Forms.TextBox();
            this.condText = new System.Windows.Forms.Label();
            this.urlText = new System.Windows.Forms.Label();
            this.textText = new System.Windows.Forms.Label();
            this.textBox = new System.Windows.Forms.TextBox();
            this.actionText = new System.Windows.Forms.Label();
            this.addressText = new System.Windows.Forms.Label();
            this.adressBox = new System.Windows.Forms.TextBox();
            this.addButton = new System.Windows.Forms.Button();
            this.urlAlarm = new System.Windows.Forms.Label();
            this.textAlarm = new System.Windows.Forms.Label();
            this.adressAlarm = new System.Windows.Forms.Label();
            this.taskListBox = new System.Windows.Forms.ListBox();
            this.actButton = new System.Windows.Forms.Button();
            this.clearButton = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.weatherButton = new System.Windows.Forms.Button();
            this.conditionTab = new System.Windows.Forms.TabControl();
            this.check_site = new System.Windows.Forms.TabPage();
            this.check_weather = new System.Windows.Forms.TabPage();
            this.cityAlarm = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tempText = new System.Windows.Forms.TextBox();
            this.townName = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.actionTab = new System.Windows.Forms.TabControl();
            this.send_to_mail = new System.Windows.Forms.TabPage();
            this.show = new System.Windows.Forms.TabPage();
            this.tempAlarm = new System.Windows.Forms.Label();
            this.conditionTab.SuspendLayout();
            this.check_site.SuspendLayout();
            this.check_weather.SuspendLayout();
            this.actionTab.SuspendLayout();
            this.send_to_mail.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // urlBox
            // 
            this.urlBox.Location = new System.Drawing.Point(71, 20);
            this.urlBox.Name = "urlBox";
            this.urlBox.Size = new System.Drawing.Size(198, 20);
            this.urlBox.TabIndex = 1;
            this.urlBox.TextChanged += new System.EventHandler(this.urlBox_TextChanged);
            // 
            // condText
            // 
            this.condText.AutoSize = true;
            this.condText.Font = new System.Drawing.Font("Monotype Corsiva", 36F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.condText.Location = new System.Drawing.Point(23, 9);
            this.condText.Name = "condText";
            this.condText.Size = new System.Drawing.Size(188, 57);
            this.condText.TabIndex = 2;
            this.condText.Text = "Jeżeli to:";
            // 
            // urlText
            // 
            this.urlText.AutoSize = true;
            this.urlText.Location = new System.Drawing.Point(12, 23);
            this.urlText.Name = "urlText";
            this.urlText.Size = new System.Drawing.Size(32, 13);
            this.urlText.TabIndex = 4;
            this.urlText.Text = "URL:";
            // 
            // textText
            // 
            this.textText.AutoSize = true;
            this.textText.Location = new System.Drawing.Point(12, 60);
            this.textText.Name = "textText";
            this.textText.Size = new System.Drawing.Size(37, 13);
            this.textText.TabIndex = 5;
            this.textText.Text = "Tekst:";
            // 
            // textBox
            // 
            this.textBox.Location = new System.Drawing.Point(71, 57);
            this.textBox.Name = "textBox";
            this.textBox.Size = new System.Drawing.Size(198, 20);
            this.textBox.TabIndex = 6;
            this.textBox.TextChanged += new System.EventHandler(this.textBox_TextChanged);
            // 
            // actionText
            // 
            this.actionText.AutoSize = true;
            this.actionText.Font = new System.Drawing.Font("Monotype Corsiva", 36F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.actionText.Location = new System.Drawing.Point(23, 197);
            this.actionText.Name = "actionText";
            this.actionText.Size = new System.Drawing.Size(286, 57);
            this.actionText.TabIndex = 7;
            this.actionText.Text = "to wykonaj to:";
            // 
            // addressText
            // 
            this.addressText.AutoSize = true;
            this.addressText.Location = new System.Drawing.Point(23, 45);
            this.addressText.Name = "addressText";
            this.addressText.Size = new System.Drawing.Size(37, 13);
            this.addressText.TabIndex = 10;
            this.addressText.Text = "Adres:";
            // 
            // adressBox
            // 
            this.adressBox.Location = new System.Drawing.Point(74, 42);
            this.adressBox.Name = "adressBox";
            this.adressBox.Size = new System.Drawing.Size(198, 20);
            this.adressBox.TabIndex = 11;
            this.adressBox.TextChanged += new System.EventHandler(this.adressBox_TextChanged);
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(586, 329);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(208, 61);
            this.addButton.TabIndex = 12;
            this.addButton.Text = "Dodaj";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // urlAlarm
            // 
            this.urlAlarm.AutoSize = true;
            this.urlAlarm.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.urlAlarm.ForeColor = System.Drawing.Color.Red;
            this.urlAlarm.Location = new System.Drawing.Point(275, 15);
            this.urlAlarm.Name = "urlAlarm";
            this.urlAlarm.Size = new System.Drawing.Size(220, 24);
            this.urlAlarm.TabIndex = 13;
            this.urlAlarm.Text = "nie podano adresu URL";
            this.urlAlarm.Visible = false;
            // 
            // textAlarm
            // 
            this.textAlarm.AutoSize = true;
            this.textAlarm.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textAlarm.ForeColor = System.Drawing.Color.Red;
            this.textAlarm.Location = new System.Drawing.Point(275, 54);
            this.textAlarm.Name = "textAlarm";
            this.textAlarm.Size = new System.Drawing.Size(222, 22);
            this.textAlarm.TabIndex = 14;
            this.textAlarm.Text = "nie podano tytułu obrazka";
            this.textAlarm.Visible = false;
            // 
            // adressAlarm
            // 
            this.adressAlarm.AutoSize = true;
            this.adressAlarm.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.adressAlarm.ForeColor = System.Drawing.Color.Red;
            this.adressAlarm.Location = new System.Drawing.Point(278, 40);
            this.adressAlarm.Name = "adressAlarm";
            this.adressAlarm.Size = new System.Drawing.Size(231, 22);
            this.adressAlarm.TabIndex = 15;
            this.adressAlarm.Text = "nie ustawiono adresu email";
            this.adressAlarm.Visible = false;
            // 
            // taskListBox
            // 
            this.taskListBox.FormattingEnabled = true;
            this.taskListBox.Location = new System.Drawing.Point(586, 48);
            this.taskListBox.Margin = new System.Windows.Forms.Padding(2);
            this.taskListBox.Name = "taskListBox";
            this.taskListBox.Size = new System.Drawing.Size(428, 186);
            this.taskListBox.TabIndex = 16;
            // 
            // actButton
            // 
            this.actButton.Location = new System.Drawing.Point(586, 245);
            this.actButton.Margin = new System.Windows.Forms.Padding(2);
            this.actButton.Name = "actButton";
            this.actButton.Size = new System.Drawing.Size(104, 68);
            this.actButton.TabIndex = 17;
            this.actButton.Text = "Wykonaj!";
            this.actButton.UseVisualStyleBackColor = true;
            this.actButton.Click += new System.EventHandler(this.actButton_Click);
            // 
            // clearButton
            // 
            this.clearButton.Location = new System.Drawing.Point(694, 245);
            this.clearButton.Margin = new System.Windows.Forms.Padding(2);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(100, 68);
            this.clearButton.TabIndex = 18;
            this.clearButton.Text = "Czyść";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(800, 245);
            this.button3.Margin = new System.Windows.Forms.Padding(2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(212, 36);
            this.button3.TabIndex = 19;
            this.button3.Text = "deserialize";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.deserializeButton_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(800, 285);
            this.button4.Margin = new System.Windows.Forms.Padding(2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(212, 28);
            this.button4.TabIndex = 20;
            this.button4.Text = "serialize";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.serializeButton_Click);
            // 
            // weatherButton
            // 
            this.weatherButton.Location = new System.Drawing.Point(800, 329);
            this.weatherButton.Name = "weatherButton";
            this.weatherButton.Size = new System.Drawing.Size(213, 61);
            this.weatherButton.TabIndex = 21;
            this.weatherButton.Text = "Pogoda";
            this.weatherButton.UseVisualStyleBackColor = true;
            this.weatherButton.Click += new System.EventHandler(this.weatherButton_Click);
            // 
            // conditionTab
            // 
            this.conditionTab.Controls.Add(this.check_site);
            this.conditionTab.Controls.Add(this.check_weather);
            this.conditionTab.Location = new System.Drawing.Point(25, 68);
            this.conditionTab.Margin = new System.Windows.Forms.Padding(2);
            this.conditionTab.Name = "conditionTab";
            this.conditionTab.SelectedIndex = 0;
            this.conditionTab.Size = new System.Drawing.Size(539, 131);
            this.conditionTab.TabIndex = 22;
            // 
            // check_site
            // 
            this.check_site.Controls.Add(this.urlBox);
            this.check_site.Controls.Add(this.urlText);
            this.check_site.Controls.Add(this.textText);
            this.check_site.Controls.Add(this.textBox);
            this.check_site.Controls.Add(this.textAlarm);
            this.check_site.Controls.Add(this.urlAlarm);
            this.check_site.Location = new System.Drawing.Point(4, 22);
            this.check_site.Margin = new System.Windows.Forms.Padding(2);
            this.check_site.Name = "check_site";
            this.check_site.Padding = new System.Windows.Forms.Padding(2);
            this.check_site.Size = new System.Drawing.Size(531, 105);
            this.check_site.TabIndex = 0;
            this.check_site.Text = "sprawdz strone";
            this.check_site.UseVisualStyleBackColor = true;
            // 
            // check_weather
            // 
            this.check_weather.Controls.Add(this.tempAlarm);
            this.check_weather.Controls.Add(this.cityAlarm);
            this.check_weather.Controls.Add(this.label2);
            this.check_weather.Controls.Add(this.label1);
            this.check_weather.Controls.Add(this.tempText);
            this.check_weather.Controls.Add(this.townName);
            this.check_weather.Controls.Add(this.menuStrip1);
            this.check_weather.Location = new System.Drawing.Point(4, 22);
            this.check_weather.Margin = new System.Windows.Forms.Padding(2);
            this.check_weather.Name = "check_weather";
            this.check_weather.Padding = new System.Windows.Forms.Padding(2);
            this.check_weather.Size = new System.Drawing.Size(531, 105);
            this.check_weather.TabIndex = 1;
            this.check_weather.Text = "sprawdz pogode";
            this.check_weather.UseVisualStyleBackColor = true;
            // 
            // cityAlarm
            // 
            this.cityAlarm.AutoSize = true;
            this.cityAlarm.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Bold);
            this.cityAlarm.ForeColor = System.Drawing.Color.Red;
            this.cityAlarm.Location = new System.Drawing.Point(275, 15);
            this.cityAlarm.Name = "cityAlarm";
            this.cityAlarm.Size = new System.Drawing.Size(182, 24);
            this.cityAlarm.TabIndex = 7;
            this.cityAlarm.Text = "nie wybrano miasta";
            this.cityAlarm.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "temp >";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Miasto:";
            // 
            // tempText
            // 
            this.tempText.Location = new System.Drawing.Point(71, 57);
            this.tempText.Name = "tempText";
            this.tempText.Size = new System.Drawing.Size(198, 20);
            this.tempText.TabIndex = 1;
            this.tempText.TextChanged += new System.EventHandler(this.tempText_TextChanged);
            // 
            // townName
            // 
            this.townName.Location = new System.Drawing.Point(71, 20);
            this.townName.Name = "townName";
            this.townName.Size = new System.Drawing.Size(198, 20);
            this.townName.TabIndex = 0;
            this.townName.TextChanged += new System.EventHandler(this.townName_TextChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Location = new System.Drawing.Point(2, 2);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(527, 24);
            this.menuStrip1.TabIndex = 6;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // actionTab
            // 
            this.actionTab.Controls.Add(this.send_to_mail);
            this.actionTab.Controls.Add(this.show);
            this.actionTab.Location = new System.Drawing.Point(33, 265);
            this.actionTab.Name = "actionTab";
            this.actionTab.SelectedIndex = 0;
            this.actionTab.Size = new System.Drawing.Size(531, 125);
            this.actionTab.TabIndex = 23;
            // 
            // send_to_mail
            // 
            this.send_to_mail.Controls.Add(this.addressText);
            this.send_to_mail.Controls.Add(this.adressBox);
            this.send_to_mail.Controls.Add(this.adressAlarm);
            this.send_to_mail.Location = new System.Drawing.Point(4, 22);
            this.send_to_mail.Name = "send_to_mail";
            this.send_to_mail.Padding = new System.Windows.Forms.Padding(3);
            this.send_to_mail.Size = new System.Drawing.Size(523, 99);
            this.send_to_mail.TabIndex = 0;
            this.send_to_mail.Text = "wyslij";
            this.send_to_mail.UseVisualStyleBackColor = true;
            // 
            // show
            // 
            this.show.Location = new System.Drawing.Point(4, 22);
            this.show.Name = "show";
            this.show.Padding = new System.Windows.Forms.Padding(3);
            this.show.Size = new System.Drawing.Size(523, 99);
            this.show.TabIndex = 1;
            this.show.Text = "wyswietl";
            this.show.UseVisualStyleBackColor = true;
            // 
            // tempAlarm
            // 
            this.tempAlarm.AutoSize = true;
            this.tempAlarm.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Bold);
            this.tempAlarm.ForeColor = System.Drawing.Color.Red;
            this.tempAlarm.Location = new System.Drawing.Point(275, 54);
            this.tempAlarm.Name = "tempAlarm";
            this.tempAlarm.Size = new System.Drawing.Size(233, 24);
            this.tempAlarm.TabIndex = 8;
            this.tempAlarm.Text = "nie wybrano temperatury";
            this.tempAlarm.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1043, 410);
            this.Controls.Add(this.actionTab);
            this.Controls.Add(this.condText);
            this.Controls.Add(this.conditionTab);
            this.Controls.Add(this.weatherButton);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.actionText);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.clearButton);
            this.Controls.Add(this.actButton);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.taskListBox);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Form1";
            this.conditionTab.ResumeLayout(false);
            this.check_site.ResumeLayout(false);
            this.check_site.PerformLayout();
            this.check_weather.ResumeLayout(false);
            this.check_weather.PerformLayout();
            this.actionTab.ResumeLayout(false);
            this.send_to_mail.ResumeLayout(false);
            this.send_to_mail.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ImageList imageList1;
        private System.ServiceProcess.ServiceController serviceController1;
        private System.Windows.Forms.TextBox urlBox;
        private System.Windows.Forms.Label condText;
        private System.Windows.Forms.Label urlText;
        private System.Windows.Forms.Label textText;
        private System.Windows.Forms.TextBox textBox;
        private System.Windows.Forms.Label actionText;
        private System.Windows.Forms.Label addressText;
        private System.Windows.Forms.TextBox adressBox;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Label urlAlarm;
        private System.Windows.Forms.Label textAlarm;
        private System.Windows.Forms.Label adressAlarm;
        private System.Windows.Forms.ListBox taskListBox;
        private System.Windows.Forms.Button actButton;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private TaskList taskList;
        private System.Windows.Forms.Button weatherButton;
        private System.Windows.Forms.TabControl conditionTab;
        private System.Windows.Forms.TabPage check_site;
        private System.Windows.Forms.TabPage check_weather;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tempText;
        private System.Windows.Forms.TextBox townName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.TabControl actionTab;
        private System.Windows.Forms.TabPage send_to_mail;
        private System.Windows.Forms.TabPage show;
        private System.Windows.Forms.Label cityAlarm;
        private System.Windows.Forms.Label tempAlarm;
    }
}

